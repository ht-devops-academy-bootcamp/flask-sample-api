# Installation:

## Locally:
### Install dependencies:
Make sure you have python installed with pip:

> sudo apt install python3-pip

Your need to create an virtual environment:

> python -m venv venv

> source venv/bin/activate

> cd src

> pip install -r requirements.txt

### Run the project:
#### Test Server:
To run the project with test server:

> python run.py

Check localhost:5000/items 

## In production:
#### with uWSGI:
To run the project with production server
> pip install -r requirements.txt

> uwsgi --ini uwsgi

Check localhost:5000/items
