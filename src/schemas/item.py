from settings import marshmllw
from models import ItemModel


class ItemSchema(marshmllw.SQLAlchemyAutoSchema):
    class Meta:
        model = ItemModel
        load_instance = True

